## 1.09

* Add **radix** and **lubridate** packages.

## 1.08

* Upgrade ubuntu to 18.10.

## 1.07

* Add **MatchIt** package.


## 1.06

* Add **jsmodule**(unreleased) package.


## 1.05

* Add **r2d3** package.

## 1.04 

* Add **pkgdown** package.


## 1.03 

* Add **metafor, roxygen2, sinew** packages.
